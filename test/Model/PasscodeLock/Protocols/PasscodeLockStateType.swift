//
//  PasscodeLockStateType.swift
//  test
//
//  Created by vmio vmio on 5/2/19.
//  Copyright © 2019 vmio vmio. All rights reserved.
//

import Foundation

public protocol PasscodeLockStateType {
    
    var title: String { get }
    var description: String { get }
    var isCancellableAction: Bool { get }
    var isTouchIDAllowed: Bool { get }
    
    mutating func accept(passcode: String, from lock: PasscodeLockType)
}
