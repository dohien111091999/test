//
//  PasscodeRepositoryType.swift
//  test
//
//  Created by vmio vmio on 5/2/19.
//  Copyright © 2019 vmio vmio. All rights reserved.
//

import Foundation

public protocol PasscodeRepositoryType {
    
    var hasPasscode: Bool { get }
    
    func save(passcode: String)
    func check(passcode: String) -> Bool
    func delete()
}
