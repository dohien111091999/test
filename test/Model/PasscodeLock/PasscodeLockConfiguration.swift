//
//  PasscodeLockConfiguration.swift
//  test
//
//  Created by vmio vmio on 5/3/19.
//  Copyright © 2019 vmio vmio. All rights reserved.
//

import Foundation

struct PasscodeLockConfiguration: PasscodeLockConfigurationType {
    let repository: PasscodeRepositoryType
    let passcodeLength = 6
    var isTouchIDAllowed = true
    let shouldRequestTouchIDImmediately = true
    let maximumIncorrectPasscodeAttempts = -1
    
    init(repository: PasscodeRepositoryType) {
        self.repository = repository
    }
    
    init() {
        self.repository = UserDefaultsPasscodeRepository()
    }
    
}
