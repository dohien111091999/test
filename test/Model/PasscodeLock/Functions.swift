//
//  Functions.swift
//  test
//
//  Created by vmio vmio on 5/2/19.
//  Copyright © 2019 vmio vmio. All rights reserved.
//

import Foundation

func localizedStringFor(key: String, comment: String) -> String {
    
    let name = "PasscodeLock"
    let defaultString = NSLocalizedString(key, tableName: name, bundle: Bundle(for: PasscodeLock.self), comment: comment)
    
    return NSLocalizedString(key, tableName: name, bundle: Bundle.main, value: defaultString, comment: comment)
}

func bundleForResource(name: String, ofType type: String) -> Bundle {
    
    if(Bundle.main.path(forResource: name, ofType: type) != nil) {
        return Bundle.main
    }
    
    return Bundle(for: PasscodeLock.self)
}
