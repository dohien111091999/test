//
//  ConfigurationHelper.swift
//  test
//
//  Created by vmio vmio on 2/11/19.
//  Copyright © 2019 vmio vmio. All rights reserved.
//

import Foundation

internal func Init<Type>(_ value: Type, block: (_ object: Type) -> Void) -> Type {
    block(value)
    return value
}
