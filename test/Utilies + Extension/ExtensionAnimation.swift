//
//  ExtensionAnimation.swift
//  test
//
//  Created by vmio vmio on 2/27/19.
//  Copyright © 2019 vmio vmio. All rights reserved.
//

import UIKit

extension MainLoveViewController {
    func animationContent() {
        // 1
        UIView.animate(withDuration: 21, delay: 1, options: [], animations: {                       //
            self.yearPhotoImage.center.y = self.view.bounds.width / 2                               //
            UIView.animate(withDuration: 2, animations: {                                           //
                self.yearPhotoImage.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)              //                   Animation YearImage
            }) {  (_) in                                                                            //
                UIView.animate(withDuration: 2, animations: {                                       //
                    self.yearPhotoImage.transform = CGAffineTransform.identity                      //
                }, completion: nil)                                                                 //
            }                                                                                       //
        }, completion: nil)                                                                         //
                                                                                                    //
        UIView.animate(withDuration: 21, delay: 1, options: [], animations: {                       //
            self.numberYearLabel.center.y = self.view.bounds.width / 2                              //                   Animation YearNumber
        }, completion: nil)                                                                         //
                                                                                                    //
        UIView.animate(withDuration: 25, delay: 1, options: [], animations: {                       //                   Animation YearLabel
            self.yearLabel.center.y = self.view.bounds.width / 2                                    //
        }, completion: nil)                                                                         //
        
        
        //2
        UIView.animate(withDuration: 21.5, delay: 1, options: [], animations: {                     //
            self.monthPhotoImage.center.y = self.view.bounds.width / 2                              //
            UIView.animate(withDuration: 2.5, animations: {                                         //
                self.monthPhotoImage.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)             //                   Animation MonthImage
            }) { (_) in                                                                             //
                UIView.animate(withDuration: 2.5, animations: {                                     //
                    self.monthPhotoImage.transform = CGAffineTransform.identity                     //
                }, completion: nil)                                                                 //
            }                                                                                       //
        }, completion: nil)                                                                         //
                                                                                                    //
        UIView.animate(withDuration: 21.5, delay: 1, options: [], animations: {                     //                  Animation MonthNumber
            self.numberMonthLabel.center.y = self.view.bounds.width / 2                             //
        }, completion: nil)                                                                         //
                                                                                                    //
        UIView.animate(withDuration: 25.5, delay: 1, options: [], animations: {                     //
            self.monthLabel.center.y  = self.view.bounds.width / 2                                  //                  Animation MonthLabel
        }, completion: nil)                                                                         //
        
        
        //3
        UIView.animate(withDuration: 22, delay: 1, options: [], animations: {                      //
            self.weekPhotoImage.center.y = self.view.bounds.width / 2                              //
            UIView.animate(withDuration: 3, animations: {                                          //
                self.weekPhotoImage.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)             //                   Animation WeekImage
            }) { (_) in                                                                            //
                UIView.animate(withDuration: 3, animations: {                                      //
                    self.weekPhotoImage.transform = CGAffineTransform.identity                     //
                }, completion: nil)                                                                //
            }                                                                                      //
        }, completion: nil)                                                                        //
                                                                                                   //
        UIView.animate(withDuration: 22, delay: 1, options: [], animations: {                      //                   Animation WeekNumber
            self.numberWeekLabel.center.y = self.view.bounds.width / 2                             //
        }, completion: nil)                                                                        //
                                                                                                   //
        UIView.animate(withDuration: 26, delay: 1, options: [], animations: {                      //                   Animation WekkLabel
            self.weekLabel.center.y = self.view.bounds.width / 2                                   //
        }, completion: nil)                                                                        //
        
        
        //4
        UIView.animate(withDuration: 22.5, delay: 1, options: [], animations: {                     //
            self.dayPhotoImage.center.y = self.view.bounds.width / 2                                //
            UIView.animate(withDuration: 3.5, animations: {                                         //
                self.dayPhotoImage.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)               //                   Animation DayImage
            }) { (_) in                                                                             //
                UIView.animate(withDuration: 3.5, animations: {                                     //
                    self.dayPhotoImage.transform = CGAffineTransform.identity                       //
                }, completion: nil)                                                                 //
            }                                                                                       //
        }, completion: nil)                                                                         //
                                                                                                    //
        UIView.animate(withDuration: 22.5, delay: 1, options: [], animations: {                     //
            self.numberDayLabel.center.y = self.view.bounds.width / 2                               //                   Animation DayNumber
        }, completion: nil)                                                                         //
                                                                                                    //
        UIView.animate(withDuration: 26.5, delay: 1, options: [], animations: {                     //
            self.dayLabel.center.y = self.view.bounds.width / 2                                     //                   Animation DayLabel
        }, completion: nil)                                                                         //
        
        
        //5
        UIView.animate(withDuration: 23, delay: 1, options: [], animations: {                      //
            self.hourPhotoImage.center.y = self.view.bounds.width / 2                              //
            UIView.animate(withDuration: 4, animations: {                                          //
                self.hourPhotoImage.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)             //                   Animation HourImage
            }) { (_) in                                                                            //
                UIView.animate(withDuration: 4, animations: {                                      //
                    self.hourPhotoImage.transform = CGAffineTransform.identity                     //
                }, completion: nil)                                                                //
            }                                                                                      //
        }, completion: nil)                                                                        //
                                                                                                   //
        UIView.animate(withDuration: 23, delay: 1, options: [], animations: {                      //
            self.numberHourLabel.center.y = self.view.bounds.width / 2                             //                    Animation HourNumber
        }, completion: nil)                                                                        //
                                                                                                   //
        UIView.animate(withDuration: 27, delay: 1, options: [], animations: {                      //
            self.hourLabel.center.y = self.view.bounds.width / 2                                   //                    Animation HourLabel
        }, completion: nil)                                                                        //
        
        //6
        UIView.animate(withDuration: 23.5, delay: 1, options: [], animations: {                     //
            self.minutePhotoImage.center.y = self.view.bounds.width / 2                             //
            UIView.animate(withDuration: 4.5, animations: {                                         //
                self.minutePhotoImage.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)            //                     Animation MinuteImage
            }) { (_) in                                                                             //
                UIView.animate(withDuration: 4.5, animations: {                                     //
                    self.minutePhotoImage.transform = CGAffineTransform.identity                    //
                }, completion: nil)                                                                 //
            }                                                                                       //
        }, completion: nil)                                                                         //
                                                                                                    //
        UIView.animate(withDuration: 23.5, delay: 1, options: [], animations: {                     //                     Animation MinuteNumber
            self.numberMinuteLabel.center.y = self.view.bounds.width / 2                            //
        }, completion: nil)                                                                         //
                                                                                                    //
        UIView.animate(withDuration: 27.5, delay: 1, options: [], animations: {                     //
            self.minuteLabel.center.y = self.view.bounds.width / 2                                  //                     Animation MinuteLabel
        }, completion: nil)                                                                         //
        
        //7
        UIView.animate(withDuration: 24, delay: 1, options: [], animations: {                      //
            self.secondPhotoImage.center.y = self.view.bounds.width / 2                            //
            UIView.animate(withDuration: 5, animations: {                                          //                     Animation SecondImage
                self.secondPhotoImage.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)           //
            }) { (_) in                                                                            //
                UIView.animate(withDuration: 5, animations: {                                      //
                    self.secondPhotoImage.transform = CGAffineTransform.identity                   //
                }, completion: nil)                                                                //
            }                                                                                      //
        }, completion: nil)                                                                        //
                                                                                                   //
        UIView.animate(withDuration: 24, delay: 1, options: [], animations: {                      //                     Animation SecondNumber
            self.numberSecondLabel.center.y = self.view.bounds.width / 2                           //
        }, completion: nil)                                                                        //
                                                                                                   //
        UIView.animate(withDuration: 28, delay: 1, options: [], animations: {                      //
            self.secondLabel.center.y = self.view.bounds.width / 2                                 //                     Animation SecondLabel
        }, completion: nil)                                                                        //
        
        
        UIView.animate(withDuration: 20, delay: 0.5, options: [], animations: {
            self.nameAppLabel.center.y = self.view.bounds.width - 200
        }, completion: nil)
    }
}
