//
//  ViewController.swift
//  test
//
//  Created by vmio vmio on 1/11/19.
//  Copyright © 2019 vmio vmio. All rights reserved.
//

import UIKit
import AVFoundation
import SideMenu
import SafariServices
import Photos

extension String {
    func dateConvert(format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
        let date = dateFormatter.date(from: self)
        return date
    }
}

class MainLoveViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

//    @IBOutlet weak var videoLove: UIView!
    @IBOutlet weak var dateCenterLabel: UILabel!
    @IBOutlet weak var backgroundImagePhoto: UIImageView!
    
    @IBOutlet weak var numberYearLabel: UILabel!
    @IBOutlet weak var numberMonthLabel: UILabel!
    @IBOutlet weak var numberWeekLabel: UILabel!
    @IBOutlet weak var numberDayLabel: UILabel!
    @IBOutlet weak var numberHourLabel: UILabel!
    @IBOutlet weak var numberMinuteLabel: UILabel!
    @IBOutlet weak var numberSecondLabel: UILabel!
    
    @IBOutlet weak var yearPhotoImage: UIImageView!
    @IBOutlet weak var monthPhotoImage: UIImageView!
    @IBOutlet weak var weekPhotoImage: UIImageView!
    @IBOutlet weak var dayPhotoImage: UIImageView!
    @IBOutlet weak var hourPhotoImage: UIImageView!
    @IBOutlet weak var minutePhotoImage: UIImageView!
    @IBOutlet weak var secondPhotoImage: UIImageView!
    
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var weekLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var minuteLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    
    @IBOutlet weak var loveCenterPhotoImage: UIImageView!
    @IBOutlet weak var imageAnimationLeftPhoto: UIImageView!
    @IBOutlet weak var imageAnimationRightPhoto: UIImageView!
    @IBOutlet weak var mainLoaveImagePhoto: UIImageView!
    @IBOutlet weak var viewBeenLoove: UIView!
    @IBOutlet weak var nameAppLabel: UILabel!

    var timerAnimation: Timer?
    var imagePickerCheck = true
    var releaseDate: Date?
    var player : AVPlayer!
    var avPlayerLayer : AVPlayerLayer!
    
    let currentDateTime = Date()
    var dateTime: String?
    

    fileprivate var alertStyle: UIAlertController.Style = .actionSheet
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImagePhoto.image = UIImage(named: "backgroundImage")
        if let dateToday = UserDefaults.standard.string(forKey: "dateString"){

            let date = dateToday.dateConvert(format: "yyyy-MM-dd HH:mm:ss")

            let calendar = Calendar.current
            let date1 = calendar.startOfDay(for: self.currentDateTime)
            let date2 = calendar.startOfDay(for: date!)

            let components = calendar.dateComponents([.day], from: date2, to: date1)
            self.dateCenterLabel.text = components.day!.description + "Days"
            Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countDownDate), userInfo: nil, repeats: true)
        }

//        videoLove.isHidden = true
//        playVideo()
        // Do any additional setup after loading the view.
//        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        
        timerAnimation = Timer.scheduledTimer(timeInterval: 0.2, target: self,
                                              selector: #selector(animationSearch), userInfo: nil, repeats: true)
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuAnimationTransformScaleFactor = 0.5
        setupSideMenu()
        animationContent()
        
    }
    
//    private func playVideo() {
//        guard let path = Bundle.main.path(forResource: "videoLove", ofType:"mp4") else {
//            debugPrint("video.m4v not found")
//            return
//        }
//        player = AVPlayer(url: URL(fileURLWithPath: path))
//        avPlayerLayer = AVPlayerLayer(player: player)
//        avPlayerLayer.videoGravity = AVLayerVideoGravity.resize
//
//        videoLove.layer.addSublayer(avPlayerLayer)
//        player.play()
//
//    }
    
//    @objc func playerDidFinishPlaying(note: NSNotification) {
//        print("Video Finished")
//        videoLove.isHidden = true
//    }
//
//    override func viewDidLayoutSubviews() {
//        avPlayerLayer.frame = videoLove.layer.bounds
//    }
    
    @IBAction func sideRightMenuButton(_ sender: Any) {
        
    }
    
    @IBAction func dayTimeButton(_ sender: Any) {
        let alert = UIAlertController(title: "Function", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Change date start", style: .default , handler:{ (UIAlertAction)in
            print("User click Change date start button")
            
            let alert = UIAlertController(title: "Date", message: nil, preferredStyle: .actionSheet)
            alert.addDatePicker(mode: .date, date: Date(), action: { (date) in
                
                let outputDateFormatter = DateFormatter()
                outputDateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
                let dateString = outputDateFormatter.string(from: date)
                UserDefaults.standard.set(dateString, forKey: "dateString")
                
                let calendar = Calendar.current
                let date1 = calendar.startOfDay(for: self.currentDateTime)
                let date2 = calendar.startOfDay(for: date)
                
                let components = calendar.dateComponents([.day], from: date2, to: date1)
                self.dateCenterLabel.text = components.day!.description + "Days"
                Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countDownDate), userInfo: nil, repeats: true)
            })
            alert.addAction(title: "Done", style: .cancel)
            self.present(alert, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Change top title", style: .default , handler:{ (UIAlertAction) in
            print("User click Change top title button")
            let alertTextFieldTop = UIAlertController(style: self.alertStyle, title: "TextField", message: "Change top title")
            
            let textField: TextField.Config = { textField in
                textField.left(image: #imageLiteral(resourceName: "pen"), color: .black)
                textField.leftViewPadding = 12
                textField.becomeFirstResponder()
                textField.borderWidth = 1
                textField.cornerRadius = 8
                textField.borderColorText = UIColor.lightGray.withAlphaComponent(0.5)
                textField.backgroundColor = nil
                textField.textColor = .black
                textField.placeholder = "Type something"
                textField.keyboardAppearance = .default
                textField.keyboardType = .default
                //textField.isSecureTextEntry = true
                textField.returnKeyType = .done
                textField.action { textField in
                    Log("textField = \(String(describing: textField.text))")
                }
            }
            
            alertTextFieldTop.addOneTextField(configuration: textField)
            alertTextFieldTop.addAction(title: "OK", style: .cancel)
            alertTextFieldTop.show()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Change bottom title", style: .default , handler:{ (UIAlertAction)in
            print("User click Change bottom title button")
            
            let alertTextFieldBottom = UIAlertController(style: self.alertStyle, title: "TextField", message: "Change top title")
            
            let textField: TextField.Config = { textField in
                textField.left(image: #imageLiteral(resourceName: "pen"), color: .black)
                textField.leftViewPadding = 12
                textField.becomeFirstResponder()
                textField.borderWidth = 1
                textField.cornerRadius = 8
                textField.borderColorText = UIColor.lightGray.withAlphaComponent(0.5)
                textField.backgroundColor = nil
                textField.textColor = .black
                textField.placeholder = "Type something"
                textField.keyboardAppearance = .default
                textField.keyboardType = .default
                //textField.isSecureTextEntry = true
                textField.returnKeyType = .done
                textField.action { textField in
                    Log("textField = \(String(describing: textField.text))")
                }
            }
            
            alertTextFieldBottom.addOneTextField(configuration: textField)
            alertTextFieldBottom.addAction(title: "OK", style: .cancel)
            alertTextFieldBottom.show()
        }))
        
        alert.addAction(UIAlertAction(title: "Change love shape", style: .default, handler:{ (UIAlertAction)in
            print("User click Change love shape button")
            let alertTextFieldCenter = UIAlertController(style: self.alertStyle, title: "TextField", message: "Change top title")
            
            let textField: TextField.Config = { textField in
                textField.left(image: #imageLiteral(resourceName: "pen"), color: .black)
                textField.leftViewPadding = 12
                textField.becomeFirstResponder()
                textField.borderWidth = 1
                textField.cornerRadius = 8
                textField.borderColorText = UIColor.lightGray.withAlphaComponent(0.5)
                textField.backgroundColor = nil
                textField.textColor = .black
                textField.placeholder = "Type something"
                textField.keyboardAppearance = .default
                textField.keyboardType = .default
                //textField.isSecureTextEntry = true
                textField.returnKeyType = .done
                textField.action { textField in
                    Log("textField = \(String(describing: textField.text))")
                }
            }
            
            alertTextFieldCenter.addOneTextField(configuration: textField)
            alertTextFieldCenter.addAction(title: "OK", style: .cancel)
            alertTextFieldCenter.show()
        }))
        
        alert.addAction(UIAlertAction(title: "Change wallpaper", style: .default, handler:{ (UIAlertAction)in
            print("User click Change wallpaper button")
            let photo: [UIImage] = [#imageLiteral(resourceName: "AlertImage1"), #imageLiteral(resourceName: "AlertImage2"), #imageLiteral(resourceName: "AlertImage3"), #imageLiteral(resourceName: "AlertImage4"), #imageLiteral(resourceName: "AlertImage5"), #imageLiteral(resourceName: "AlertImage6"), #imageLiteral(resourceName: "AlertImage7"), #imageLiteral(resourceName: "AlertImage8"), #imageLiteral(resourceName: "AlertImage9"),#imageLiteral(resourceName: "AlertImage10"),#imageLiteral(resourceName: "AlertImage11"),#imageLiteral(resourceName: "AlertImage12")]
            let alertImage = UIAlertController(style: self.alertStyle)
            alertImage.addImagePicker(flow: .vertical, paging: false, images: photo, selection: .single(action: { image in
                self.backgroundImagePhoto.image = image
                Log(image)
                
            }))
            
            alertImage.addAction(title: "OK", style: .cancel)
            self.present(alertImage, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Close button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @IBAction func selectedImageFromPhotoLibraryRight(_ sender: Any) {
        imagePickerCheck = false
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
        
    }
    
    @IBAction func selectImageFromPhotoLibrary(_ sender: Any) {
        imagePickerCheck = true
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled.
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if imagePickerCheck == true {
            guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
                fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
            }
            
            imageAnimationLeftPhoto.contentMode = .scaleAspectFit
            imageAnimationLeftPhoto.tintColor = .lightGray
            imageAnimationLeftPhoto.image = #imageLiteral(resourceName: "Love1")
            
            let profileImage = UIImageView()
            profileImage.image = selectedImage
            profileImage.contentMode = .scaleAspectFill
            // Make a little bit smaller to show "border" image behind it
            profileImage.frame = imageAnimationLeftPhoto.bounds.insetBy(dx: 2, dy: 2)
            
            let maskImageView = UIImageView()
            maskImageView.image = #imageLiteral(resourceName: "Love1")
            maskImageView.contentMode = .scaleAspectFit
            maskImageView.frame = profileImage.bounds
            profileImage.mask = maskImageView
            imageAnimationLeftPhoto.addSubview(profileImage)
            
            // Dismiss the picker.
            dismiss(animated: true, completion: nil)
        } else {
            guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
                fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
            }
            
            imageAnimationRightPhoto.contentMode = .scaleAspectFit
            imageAnimationRightPhoto.tintColor = .lightGray
            imageAnimationRightPhoto.image = #imageLiteral(resourceName: "Love1")
            
            let profileImage = UIImageView()
            profileImage.image = selectedImage
            profileImage.contentMode = .scaleAspectFill
            // Make a little bit smaller to show "border" image behind it
            profileImage.frame = imageAnimationRightPhoto.bounds.insetBy(dx: 2, dy: 2)
            
            let maskImageView = UIImageView()
            maskImageView.image = #imageLiteral(resourceName: "Love1")
            maskImageView.contentMode = .scaleAspectFit
            maskImageView.frame = profileImage.bounds
            profileImage.mask = maskImageView
            imageAnimationRightPhoto.addSubview(profileImage)
            
            // Dismiss the picker.
            dismiss(animated: true, completion: nil)
        }
    }
    
    func senData(name: String) {
        dateCenterLabel.text = name + "Days"
    }

}

extension MainLoveViewController {
    
    func  interval(start: Date, end: String) -> Int {
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: .day, in: .era, for: start) else { return 0 }
        guard let end = currentCalendar.ordinality(of: .day, in: .era, for: Date()) else { return 0 }
        return end - start
    }
    
    
    @objc func countDownDate() {
        let date = Date()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let senDataLove = UserDefaults.standard.string(forKey: "dateString")
        releaseDate = dateFormat.date(from: senDataLove ?? "")
        
        let calendar = Calendar.current
        let diffDateComponents = calendar.dateComponents([ .year, .month , .weekOfMonth, .day, .hour, .minute, .second], from: releaseDate!, to: date)
        
        self.numberYearLabel.text = diffDateComponents.year?.description
        self.numberMonthLabel.text = diffDateComponents.month?.description
        self.numberWeekLabel.text = diffDateComponents.weekOfMonth?.description
        self.numberDayLabel.text = diffDateComponents.day?.description
        self.numberHourLabel.text = diffDateComponents.hour?.description
        self.numberMinuteLabel.text = diffDateComponents.minute?.description
        self.numberSecondLabel.text = diffDateComponents.second?.description
    }
    
    @objc func animationSearch() {
        UIView.animate(withDuration: 0.2, animations: {
            self.loveCenterPhotoImage.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }) { (_) in
            UIView.animate(withDuration: 0.2, animations: {
                self.loveCenterPhotoImage.transform = CGAffineTransform.identity
            }, completion: nil)
        }
    }
    
    fileprivate func setupSideMenu() {
        SideMenuManager.default.menuLeftNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        SideMenuManager.default.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "images")!)
    }

}


