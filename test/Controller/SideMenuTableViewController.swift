//
//  SideMenuTableViewController.swift
//  test
//
//  Created by vmio vmio on 1/23/19.
//  Copyright © 2019 vmio vmio. All rights reserved.
//

import UIKit
import SideMenu
class SideMenuTableViewController: UITableViewController {
 
    
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var passcodeSwitch: UISwitch!
    @IBOutlet weak var nameTextField: UILabel!
  
    @IBOutlet var tableview: UITableView!
    
    fileprivate var alertStyle: UIAlertController.Style = .actionSheet
    fileprivate let configuration: PasscodeLockConfigurationType

    init(configuration: PasscodeLockConfigurationType) {
        
        self.configuration = configuration
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        
        let repository = UserDefaultsPasscodeRepository()
        configuration = PasscodeLockConfiguration(repository: repository)
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updatePasscodeView()
        tableview.reloadData()
        
        guard SideMenuManager.default.menuBlurEffectStyle == nil else {
            return
        }
        
        let imageView = UIImageView(image: UIImage(named: "imageside"))
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.purple.withAlphaComponent(0.5)
        tableView.backgroundView = imageView
    }
    
    @IBAction func passCodeSwitchValueChange(_ sender: Any) {
        
        let passcodeVC: PasscodeLockViewController

        if passcodeSwitch.isOn {
            passcodeVC = PasscodeLockViewController(state: .set, configuration: configuration)

        } else {
            passcodeVC = PasscodeLockViewController(state: .remove, configuration: configuration)
        }

        present(passcodeVC, animated: true, completion: nil)
    }
    
    func updatePasscodeView() {

        let hasPasscode = configuration.repository.hasPasscode
        passcodeSwitch.isOn = hasPasscode
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath) as! UITableViewVibrantCell
        cell.blurEffectStyle = SideMenuManager.default.menuBlurEffectStyle
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            let alertColor = UIAlertController(style: self.alertStyle)
            alertColor.addColorPicker(color: UIColor(hex: 0xFF2DC6), selection: { (color) in Log(color)
            })
            alertColor.addAction(title: "Cancel", style: .cancel)
            alertColor.show()
        case 2:
            let alertColor = UIAlertController(style: self.alertStyle)
            alertColor.addColorPicker(color: UIColor(hex: 0xFF2DC6), selection: { (color) in Log(color)
            })
            alertColor.addAction(title: "Cancel", style: .cancel)
            alertColor.show()
        case 3:
            let alertColor = UIAlertController(style: self.alertStyle)
            alertColor.addColorPicker(color: UIColor(hex: 0xFF2DC6), selection: { (color) in Log(color)
            })
            alertColor.addAction(title: "Cancel", style: .cancel)
            alertColor.show()
        default:
            print("nil")
        }
    }
}
