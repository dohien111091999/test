//
//  LIHImageSlider.swift
//  test
//
//  Created by vmio vmio on 3/8/19.
//  Copyright © 2019 vmio vmio. All rights reserved.
//
import UIKit
import Foundation

open class LIHSlider: NSObject {
    
    open var sliderImages: [UIImage] = []
    open var sliderDescriptions: [String] = []
    open var descriptionColor: UIColor = UIColor.white
    open var descriptionBackgroundAlpha: CGFloat = 0.3
    open var descriptionBackgroundColor: UIColor = UIColor.black
    open var descriptionFont: UIFont = UIFont.systemFont(ofSize: 15)
    open var numberOfLinesInDescription: Int = 2
    open var transitionInterval: Double = 3.0
    open var customImageView: UIImageView?
    open var showPageIndicator: Bool = true
    open var userInteractionEnabled: Bool = true
    
    //Sliding options
    open var transitionStyle: UIPageViewController.TransitionStyle = UIPageViewController.TransitionStyle.scroll
    open var slidingOrientation: UIPageViewController.NavigationOrientation = UIPageViewController.NavigationOrientation.horizontal
    open var sliderNavigationDirection: UIPageViewController.NavigationDirection = UIPageViewController.NavigationDirection.forward
    
    public init(images: [UIImage]) {
        
        self.sliderImages = images
    }
}
