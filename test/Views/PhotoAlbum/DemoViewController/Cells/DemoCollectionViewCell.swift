//
//  DemoCollectionViewCell.swift
//  test
//
//  Created by vmio vmio on 2/11/19.
//  Copyright © 2019 vmio vmio. All rights reserved.
//

import UIKit

class DemoCollectionViewCell: BasePageCollectionCell {
    
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var customTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        customTitle.layer.shadowRadius = 2
        customTitle.layer.shadowOffset = CGSize(width: 0, height: 3)
        customTitle.layer.shadowOpacity = 0.2
    }
}
